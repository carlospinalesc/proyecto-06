using System;

namespace Ejercicio02
{
   public class contacto
    {
        private string nombre;
        private string apellidos;
        private string direccion;
        private int telefono;

        public void SetContacto(string nombre)
        {
            this.nombre = nombre;

        }

        public void saludar()
        {
            Console.WriteLine("Hola soy " + nombre);
        }
    }

    public class ProbarContacto
    {
        public static void Main()
        {
            contacto cont = new contacto();
            cont.SetContacto("Carlos");
            cont.saludar();
        }
    }
}

