using System;

namespace Ejercicio01
{
    class Persona
    {
        private string cedula;

        private string nombre;

        private string apellido;

        private int edad;

        public Persona(){
            Console.Write("Ingrese su cedula: ");
            cedula = Console.ReadLine();
            Console.Write("Ingrese su nombre: ");
            nombre = Console.ReadLine();
            Console.Write("Ingrese su apellido: ");
            apellido = Console.ReadLine();
            Console.Write("Ingrese su edad: ");
            string Linea = Console.ReadLine();
            edad = int.Parse(Linea);
        }

        public void Imprimir()
        {   Console.WriteLine("===================");
            Console.WriteLine("Bienvenido " + nombre);
            Console.WriteLine("===================");
            Console.WriteLine("Cedula: " + cedula);
            Console.WriteLine("Nombre: " + nombre);
            Console.WriteLine("Apellido: " + apellido);
            Console.WriteLine("Edad: " + edad);
        }

     class Profesor  
        {
           private double Sueldo;

           public Profesor(){
               Sueldo = 20000;
           }

           public void Imprime()
           {
             Console.WriteLine("Su sueldo es: " + Sueldo);  
           }

        }

        static void Main(string[] args)
        {
            Persona Persona01 = new Persona();
            Profesor Profesor01 = new Profesor();

            Persona01.Imprimir();
            Profesor01.Imprime();
        }
    }
}